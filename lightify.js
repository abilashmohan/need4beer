var lightify = require("node-lightify");
var exec = require("child_process").exec;
var utils = require("util");
var os = require("os");

var lightCluster = null;
var timerObj = null;
var ipAddress = "10.0.10.50";
var cpuCores = os.cpus().length;

var setupLightify = (ipAddress) => {
    if (ipAddress) {
        return lightify.start(ipAddress)
            .then(lightify.discovery.bind(lightify))
            .then(el => { console.log(el); return el; })
            .then(el => el.result)
            .then(el => { console.log(el.map(el2 => el2.type)); return el.filter(el2 => lightify.isLight(el2.type)); })
            .then(el => lightCluster = el.map(el2 => el2.mac))
            //.then(setupLoop)
            .catch(console.error);
    } else {
        return new Promise((res, rej) => rej(new Error("IP Address not provided!")));
    }
};
var drunkLight = function (drunkVal) {
    var green = 0;
    drunkVal = drunkVal > 1 ? 1 : drunkVal;
    var blue = Math.round(255 * drunkVal);
    if (blue > 255) blue = 255;
    var red = 255 - blue;

    Promise.all(lightCluster.map(el => lightify.node_color(el, red, green, blue, 255, 0)))
        .catch((err) => { console.error(err); setupLightify(ipAddress); });
};
var setupLoop = function () {
    if (timerObj) {
        clearInterval(timerObj);
        timerObj = null;
    }
    console.log("Setting up the loop!");
    timerObj = setInterval(function () {
        var red = 0;
        var green = 0;
        var blue = 255;
        var distValue = 300;
        var greenValue = 250;

        // if (distance < greenValue) {
        //     var ratio = (distance - distValue) / (greenValue - distValue);
        //     green = Math.round(255 * ratio);
        // }
        if (distance <= distValue) {
            var calculate = distance / distValue;
            //calculate = calculate < 0 ? 0 : calculate;

            blue = Math.round(255 * calculate);
            red = 255 - blue;
        }
        Promise.all(lightCluster.map(el => lightify.node_color(el, red, green, blue, 255, 0)))
            .catch((err) => { console.log("error : ", err); setupLightify(ipAddress); });

    }, 150);
    // timerObj = setInterval(function () {
    //     exec("top -l 1 | head -n 4 | tail -n 1 | cut -f3 -d ',' | sed 's/ //g' | cut -f1 -d '%'",
    //         (err, stdOut, stdErr) => {
    //             if (stdOut && utils.isString(stdOut)) {
    //                 var idle = Number.parseFloat(stdOut);
    //                 if (!isNaN(idle)) {
    //                     var blue = Math.round((idle/100) * 255);
    //                     blue = blue > 255 ? 255 : blue;
    //                     var red = 255 - blue;
    //                     var green = 0;
            
    //                     console.log("Idle : ", idle);
    //                     Promise.all(lightCluster.map(el => lightify.node_color(el, red, green, blue, 255,10)))
    //                         .catch((err) => { console.log("error : ", err); setupLightify(ipAddress); });
    //                 } else {
    //                     console.log("Error executing the OSX Temp tool...");
    //                 }
    //             } else {
    //                 console.log("Oh Oh, something went wrong while executing... : ", stdOut, stdErr, " : ", err);
    //             }
    //     });

        // var load = os.loadavg()[0];
        // var red = Math.round((load / cpuCores) * 255);
        // var green = (red > 255) ? red - 255 : 0;
        // red = red > 255 ? 255 : red;
        // green = green > 255 ? 255 : green;
        // var blue = 255 - red;
        // console.log(load, red, green, blue);
        // Promise.all(lightCluster.map(el => lightify.node_color(el, red, green, blue,255, 10)))
        //     .catch(err => { console.log("Error : ", err); setupLightify(ipAddress) });

    // }, 1000);
};

var distance = Infinity;
var startItUp = () => setupLightify(ipAddress);

module.exports.startItUp = startItUp;
module.exports.setDistance = (dist) => { distance = dist; };
module.exports.drunkLight = drunkLight;

