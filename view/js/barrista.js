/*global sobriety */
var coffeeBar = {

    preload: function (gameEngine) {
        gameEngine.load.image("coffee", "assets/coffee.png");
    },

    create: function (game) {
        this.coffees = game.add.group();
    },

    update: function (game,bird) {
        game.physics.arcade.overlap(bird, this.coffees, this.drankExpresso, null,this);
        return;
    },

    drankExpresso: function (bird, coffee) {
        sobriety.hadCoffee();
        coffee.destroy();
    },
    coffeeTimerFunction: function (game,startX) {
        if (Math.random() > 0.9) {
            var expresso = game.add.sprite(startX, game.world.randomY, "coffee");

            game.physics.arcade.enable(expresso);
            expresso.body.velocity.x = Math.round(- 300 * Math.random());
            expresso.checkWorldBounds = true;
            expresso.outOfBoundsKill = true;
            this.coffees.add(expresso);
        }
    }
    
};