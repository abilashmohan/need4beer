/*global sobriety */
var beerEngine = {
    beerCount : 0,
    preload: function (gameEngine) {
        gameEngine.load.image("beer", "assets/beer.png");
    },

    create: function (game) {
        this.beers = game.add.group();
        this.beerCount = 0;
    },

    update: function (game,bird) {
        game.physics.arcade.overlap(bird, this.beers, this.drankBeer, null,this);
        return;
    },
    scoreUpdater: function () {
        return;
    },
    drankBeer: function (bird, beer) {
        sobriety.hadBeer();
        beer.destroy();
        this.beerCount++;
        if (this.scoreUpdater) this.scoreUpdater();
    },
    beerTimerFunction: function (game,startX) {
        if (Math.random() > 0.6) {
            var beer = game.add.sprite(startX, game.world.randomY, "beer");

            game.physics.arcade.enable(beer);
            beer.body.velocity.x = Math.round(-100 -(200 * Math.random()));
            beer.checkWorldBounds = true;
            beer.outOfBoundsKill = true;
            this.beers.add(beer);
        }
    }

    
};