/* global Phaser io*/
// Create our 'main' state that will contain the game
var score = 0;
var x = 800;
var y = 600;
var socket = io();

var WebFontConfig = {

    //  'active' means all requested fonts have finished loading
    //  We set a 1 second delay before calling 'createText'.
    //  For some reason if we don't the browser cannot render the text the first time it's created.
    active: function() { game.time.events.add(Phaser.Timer.SECOND, createText, this); },

    //  The Google Fonts we want to load (specify as many as you like in the array)
    google: {
        families: ["Revalia"]
    }

};
var text = null;
var grd;
var createText = function () { 
    text = game.add.text(100, 50, "Beers : " + beerEngine.beerCount);
    text.anchor.setTo(0.5);

    text.font = "Revalia";
    text.fontSize = 12;

    //  x0, y0 - x1, y1
    grd = text.context.createLinearGradient(0, 0, 0, text.canvas.height);
    grd.addColorStop(0, "#8ED6FF");   
    grd.addColorStop(1, "#004CB3");
    text.fill = grd;

    text.align = "center";
    text.stroke = "#000000";
    text.strokeThickness = 2;
    //text.setShadow(5, 5, "rgba(0,0,0,0.5)", 5);
    function updateScore() {
        text.text = "Beers : " + beerEngine.beerCount;
    }
    beerEngine.scoreUpdater = updateScore;

    game.time.events.loop(Phaser.Timer.SECOND, updateScore,this);

};


var informServerOnDrunkness = function () {
    socket.emit("sobriety", sobriety.jumpConsistency);
}

function reset() {
    socket.emit("reset", {});
    sobriety.jumpConsistency = 1;
}

var mainState = {
    preload: function () {
        // This function will be executed at the beginning     
        // That's where we load the images and sounds 
        game.load.spritesheet("bird", "assets/sandy.png",60,60);
        game.load.image("pipe", "assets/brick.png");
        game.load.image("cloud", "assets/clouds.png");
        game.load.audio("jump", "assets/jump.wav");
        game.load.image("LOR", "assets/legndOfRock.png");
        beerEngine.preload(game);
        coffeeBar.preload(game);

        //  Load the Google WebFont Loader script
        game.load.script("webfont", "//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js");

    },

    create: function () {
        // This function is called after the preload function     
        // Here we set up the game, display sprites, etc.
        // Change the background color of the game to blue
        game.stage.backgroundColor = "#71c5cf";
        this.bgTile = game.add.tileSprite(0, 0,x,y,"LOR");

        // Set the physics system
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // Display the bird at the position x=100 and y=245
        this.bird = game.add.sprite(100, game.world.centerY, "bird", 2);

        // Add physics to the bird
        // Needed for: movements, gravity, collisions, etc.
        game.physics.arcade.enable(this.bird);

        // Add gravity to the bird to make it fall
        this.bird.body.gravity.y = 1000;
        
       
        // Call the 'jump' function when the spacekey is hit
        var spaceKey = game.input.keyboard.addKey(
            Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this);
        
        // Create an empty group
        this.pipes = game.add.group();
        this.clouds = game.add.group();

        //Adding rows of pipes
        
        this.timer = game.time.events.loop(1500, this.addRowOfPipes, this);
        this.scoreTimer = game.time.events.loop(1000, this.addScore, this);
        this.telemetryTimer = game.time.events.loop(250, this.distanceEmit, this);

        // Move the anchor to the left and downward
        this.bird.anchor.setTo(-0.2, 0.5);
        //this.jumpSound = game.add.audio("jump");

        beerEngine.create(game);
        coffeeBar.create(game);
        reset();
        var decideSprite = this.decideSprite.bind(this);
        sobriety.coffeCallback = sobriety.beerCallback = function () {
            decideSprite();
            informServerOnDrunkness();
        };
    },

    update: function () {
        // This function is called 60 times per second    
        // It contains the game's logic

        // If the bird is out of the screen (too high or too low)
        // Call the 'restartGame' function
        if (this.bird.y > y)
            this.hitPipe();
        if (this.bird.angle < 20)
            this.bird.angle += 1;
        game.physics.arcade.overlap(
            this.bird, this.pipes, this.hitPipe, null, this);
        beerEngine.update(game, this.bird);
        coffeeBar.update(game, this.bird);
        sobriety.update(game, this.bird);
        this.bgTile.tilePosition.x -= 1;
    },

    // Make the bird jump 
    jump: function () {
        if (this.bird.alive == false)
            return;
        if (this.bird.y <= 0)
            return;    
        // Add a vertical velocity to the bird
        sobriety.jump(game,this.bird);
        this.bird.body.velocity.y = Math.floor(-y * 0.70 + (Math.random() * 250 * (1 - sobriety.jumpConsistency)));
        // Create an animation on the bird
        var animation = game.add.tween(this.bird);
        
        // Change the angle of the bird to -20° in 100 milliseconds
        animation.to({ angle: -20 }, 100);

        // And start the animation
        animation.start();
        //this.jumpSound.play();
        //Slowdown as a result.
        
    },

    // Restart the game
    restartGame: function() {
    // Start the 'main' state, which restarts the game
        sobriety.reset();
        game.state.start("main");
    },
    addOnePipe: function(x, y) {
        // Create a pipe at the position x and y
        var pipe = game.add.sprite(x, y, "pipe");

        // Add the pipe to our previously created group
        this.pipes.add(pipe);
        
        // Enable physics on the pipe 
        game.physics.arcade.enable(pipe);

        // Add velocity to the pipe to make it move left
        pipe.body.velocity.x = -200; 

        // Automatically kill the pipe when it's no longer visible 
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;
    },
    addRowOfPipes: function() {
        // Randomly pick a number between 1 and 5
        // This will be the hole position
        var hole = Math.floor(Math.random() * 8-holeSize) + 1;

        // Add the 6 pipes 
        // With one big hole at position 'hole' and 'hole + 1'
        for (var i = 0; i < blockCount; i++)
            if(i < hole || i > hole + holeSize)
                this.addOnePipe(x, i * 60 + blockOffset);   
        
        // //Add Clouds if necessary
        // if (Math.random() > 0.7) {
        //     console.log("Adding Cloud");
        //     var cloud = this.add.sprite(
        //         x, Math.round(Math.random() * (y /2)), "cloud"
        //     );
        //     game.physics.arcade.enable(cloud);
        //     cloud.body.velocity.x = Math.round(-180 * Math.random());
        //     cloud.checkWorldBounds = true;
        //     cloud.outOfBoundsKill = true;
        //     cloud.sendToBack();
        //     this.clouds.add(cloud);
        // }

        beerEngine.beerTimerFunction(game, x);
        coffeeBar.coffeeTimerFunction(game, x);
    },
    hitPipe: function() {
        // If the bird has already hit a pipe, do nothing
        // It means the bird is already falling off the screen
        if (this.bird.alive == false) {
            if (this.bird.y > y)
                this.bird.body.gravity.y = 0;
            return;
        }
        // Set the alive property of the bird to false
        this.bird.alive = false;
        this.bird.angle = 135;

        // Prevent new pipes from appearing
        game.time.events.remove(this.timer);
        game.time.events.remove(this.scoreTimer);

        // Go through all the pipes, and stop their movement
        this.pipes.forEach(function(p){
            p.body.velocity.x = 0;
        }, this);

        game.time.events.add(Phaser.Timer.SECOND * 3, this.restartGame, this);
    },
    addScore: function () {
        if (!this.bird.alive) { return; }
        else { score ++; }
        //console.log("Score : ", score);
    },
    distanceEmit: function () {
        var distances = [];
        var bird = this.bird;
        this.pipes.forEach(function (el) {
            if(el.x >= bird.x - 30)
                distances.push(game.physics.arcade.distanceBetween(bird, el,true));
        });
        var minDistance = distances.reduce(function (prev, curr) {
            return Math.min(prev, curr);
        }, Infinity);
        socket.emit("distance", minDistance);
    },
    decideSprite: function () {
        if (sobriety.jumpConsistency > 0.7) {
            this.bird.frame = 2;
        } else if (sobriety.jumpConsistency > 0.4) {
            this.bird.frame = 1;
        } else if(this.bird.frame != 0) {
            this.bird.frame = 0;
        }
    }
};

// Initialize Phaser, and create a 400px by 490px game
var game = new Phaser.Game(x, y,Phaser.AUTO,"game_div");
var blockCount = Math.floor(y / 60);
var blockOffset = Math.floor((y % 60) / 2);
var holeSize = Math.round(0.50 * blockCount);

// Add the 'mainState' and call it 'main'
game.state.add("main", mainState); 

// Start the state to actually start the game
game.state.start("main");
