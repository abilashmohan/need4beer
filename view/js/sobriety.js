var sobriety = {
    jumpConsistency: 1,
    beerCallback: null,
    coffeCallback: null,
    velocityDirection: 1,
    reset: function () {
        this.jumpConsistency = 1;
        this.velocityDirection = 1;
    },
    hadBeer: function () {
        if (this.jumpConsistency > 0.15) this.jumpConsistency -= 0.15;
        if (this.beerCallback) { this.beerCallback(this.jumpConsistency); }
    },
    hadCoffee: function () {
        if (this.jumpConsistency <= 1) { this.jumpConsistency += 0.15; }
        if (this.coffeCallback) { this.coffeCallback(this.jumpConsistency); }
    },

    jump: function (game, bird) {
        bird.body.velocity.x = Math.floor((1 - this.jumpConsistency) * 50) * this.velocityDirection;
    },
    update: function (game, bird) {
        if (bird.x > 600) {
            this.velocityDirection = -1;
            bird.body.velocity.x = 0;
        }else if (bird.x < 60) {
            bird.x = 60;
            bird.body.velocity.x = 0;
        } else if (bird.x < 100) {
            //bird.body.velocity.x = -bird.body.velocity.x;
            bird.body.velocity.x = 0;
            this.velocityDirection = 1;
        } 
    }
};