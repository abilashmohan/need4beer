"use strict";
var express = require("express");
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var lightify = require("./lightify");

var connected = false;

io.on("connection", (socket) => {
    connected = true;
    // socket.on("distance", (distance) => {
    //     if (distance != null) {
    //         lightify.setDistance(distance);
    //     }
    // });
    socket.on("sobriety", lightify.drunkLight);
    socket.on("reset", () => lightify.drunkLight(1));
});
lightify.startItUp();

app.use(express.static("view"));
http.listen(3000, function(){
    console.log("listening on *:3000");
});
